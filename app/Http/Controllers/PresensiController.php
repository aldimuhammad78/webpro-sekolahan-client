<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;
use App\Model\Presensi;

class PresensiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {               
        $response = Http::withToken(session()->get('tokenUser'))
                    ->get(env("REST_API_ENDPOINT").'/api/presensi');
        $dataResponse = json_decode($response);

        $this->data['presensiData'] = $dataResponse->data;
         //dd($this->data['jadwals']);
        return view('presensi.index', $this->data);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $responseKelas = Http::withToken(session()->get('tokenUser'))
                            ->get(env("REST_API_ENDPOINT").'/api/kelas');
        $responseSiswa = Http::withToken(session()->get('tokenUser'))
                            ->get(env("REST_API_ENDPOINT").'/api/siswa');
        $dataKelas = json_decode($responseKelas);
        $dataSiswa = json_decode($responseSiswa);


        $this->data['dataKelas'] = $dataKelas->data;
        $this->data['siswas'] = $dataSiswa->data;

        return view('presensi.prs.create',$this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = Http::withToken(session('tokenUser'))
                        ->post(
                            env("REST_API_ENDPOINT").'/api/presensi', $request->except('_token'));
        $data = json_decode($response);
        //dd($data);
        if ($data->status == true) {
            return redirect()->route('presensi.index')->with('success','Data presensi berhasil ditambahkan!');
        } else {
            return redirect()->route('presensi.create')->with('ValidationErrors',$data->message);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //return view('presensi.index1');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $response = Http::withToken(session()->get('tokenUser'))
                    ->get(env("REST_API_ENDPOINT").'/api/presensi/'.$id);
        $dataResponse = json_decode($response);

        $responseDataDepedenciesKelas = Http::withToken(session()->get('tokenUser'))
                                ->get(env("REST_API_ENDPOINT").'/api/kelas');
        $dataDepedenciesKelas = json_decode($responseDataDepedenciesKelas);

        $responseDataDepedenciesSiswa = Http::withToken(session()->get('tokenUser'))
                                ->get(env("REST_API_ENDPOINT").'/api/siswa');
        $dataDepedenciesSiswa = json_decode($responseDataDepedenciesSiswa);

        if ($dataResponse->status == true) {
            $presensi = $dataResponse->data;
            //dd($presensi);
            $this->data['presensiData'] = $presensi;
            $this->data['dataKelas'] = $dataDepedenciesKelas->data;
            $this->data['siswas'] = $dataDepedenciesSiswa->data;

            return view('presensi.edit',$this->data);
        } else {
            return redirect()->route('presensi.index')->with('danger','Data presensi tidak ditemukan!');
        } 

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $response = Http::withToken(session('tokenUser'))
                    ->put(env("REST_API_ENDPOINT").'/api/presensi/'.$id,
                    $request->except(['_token','_method'])
                );
        $data = json_decode($response);
        //dd($data);
        if ($data->status == true) {
            return redirect()->route('presensi.index')->with('success','Data Presensi Berhasil diubah!');
        } else {
            return redirect()->route('presensi.edit',$id)->with('ValidationErrors',$data->message);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response = Http::withToken(session('tokenUser'))
                   ->delete(env("REST_API_ENDPOINT").'/api/presensi/'.$id);
        $data = json_decode($response);

        if ($data->status == true) {
            return redirect()->route('presensi.index')->with('success','Data user berhasil dihapus!');
        } else {
            return redirect()->route('presensi.index')->with('ValidationErrors',$data->message);
        }
    } 
    public function main()
    {
         $response = Http::withToken(session()->get('tokenUser'))
                    ->get(env("REST_API_ENDPOINT").'/api/presensi');
        $dataResponse = json_decode($response);
       // dd($dataResponse);
        $this->data['presensiData'] = $dataResponse->data;
        return view('presensi.prs.index',$this->data);

    }
}


