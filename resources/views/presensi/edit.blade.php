@extends('layouts/main-admin')

@section('title', 'Edit Presensi')

@section('container')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h2 class="m-0 text-dark">Presensi</h2>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active">Edit Presensi</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<section class="container-fluid">
    <div class="card">
        @include ('includes.flash')
        <div class="card-body">
            <form role="form" method="post" action="{{ route('presensi.update',$presensiData->id)}}" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="card-body">                    
                    <div class="form-group">
                        <label for="exampleInputJK">Kelas</label>
                        <select class="form-control" name="kelas_id" id="kelas_id" required>
                            @foreach ($dataKelas as $kelas)
                            <option value="{{$kelas->id}}" @if($kelas->id == $presensiData->kelas_id)
                                selected=""
                                @endif> {{$kelas->nama_kelas}}
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputJK">Siswa</label>
                        <select class="form-control" name="siswa_id" id="siswa_id" required>
                            @foreach ($siswas as $siswa)
                            <option value="{{$siswa->id}}" @if($siswa->id == $presensiData->siswa_id)
                                selected=""
                                @endif> {{$siswa->nama}}</option>
                            @endforeach
                        </select>
                    </div>
                     <div class="form-group">
                        <label for="exampleInputJK">Absensi</label>
                        <select class="form-control" name="absensi" id="absensi" required>
                            <option value="{{$presensiData->absensi}}">Pilih</option>
                            <option value="hadir">Present</option>
                            <option value="izin">Izin</option>
                            <option value="absen">Absen</option>
                            <option value="telat">Telat</option>
                        </select>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Keterangan</label>
                        <input type="text" class="form-control" name="keterangan" id="keterangan" value="{{$presensiData->keterangan}}" required>
                    </div>                                                         
                    <div class="card-body">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
@include ('includes.scripts')
@endsection