@extends('layouts.main-admin')

@section('title', 'Presensi')

@section('container')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h2 class="m-0 text-dark">PRESENSI</h2>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active"><a href="{{route('presensi.index')}}">Presensi</a>
                </li>
                </ol>
            </div>
        </div>
    </div>
</div>

<section class="container-fluid">
    <div class="card">
        @include ('includes.flash')
        <div class="card-body">
            <table id="data-admin" class="table table-bordered table-striped">
                <thead>
                    <tr>
                    <th width="40">NO</th>
                    <th>NIS</th>
                    <th>NAMA SISWA</th>
                    <th>PRESENSI</th>
                    <th>KETERANGAN</th>
                    <th width="120">AKSI</th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($presensiData))
                        @foreach ($presensiData as $key => $presensi)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>{{$presensi->nis }}</td>
                            <td>{{ $presensi->nama}}</td>
                            <td>{{ $presensi-> absensi}}</td>
                            <td>{{ $presensi->keterangan }}</td>
                            
                            <td class="text-center">
                              <a href="{{ route('presensi.edit',$presensi->id) }}">
                                  <button class="btn btn-secondary" data-toogle="tooltip" data-placement="top" title="Ubah">
                                      <i class="fa fa-edit"></i>
                                  </button>
                              </a>  
                              <form id="delete-presensi-{{$presensi->id}}" action="/presensi/{{$presensi->id}}" method="post"
                                style="display: inline;">
                                    @method('DELETE')
                                    @csrf
                                    <button class="btn btn-danger" data-toogle="tooltip" data-placement="top" title="Hapus">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                     @endif
                </tbody>
            </table>
        </div>
    </div>
</section>
@include ('includes.scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $("#data-admin_length").append('<a  href="{{ route('presensi.create') }}"> <button type="button" class="btn btn-outline-primary ml-3">Tambah</button></a>');
        });
    </script>
@endsection